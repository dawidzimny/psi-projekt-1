var albums = [
	[ "Taylor Swift", "Taylor Swift", "2006", "Big Machine" ],
	[ "Fearless", "Taylor Swift", "2008", "Big Machine" ],
	[ "Speak Now", "Taylor Swift", "2010", "Big Machine" ],
	[ "Red", "Taylor Swift", "2012", "Big Machine" ],
	[ "1989", "Taylor Swift", "2014", "Big Machine" ],
	[ "+", "Ed Sheeran", "2011", "Asylum Records, Atlantic Records" ],
	[ "x", "Ed Sheeran", "2014", "Asylum Records, Atlantic Records" ]
];

$(document).ready(function() {
	
	$('#nav > ul > li ul').each(function(index, element){
		var count = $(element).find('li').length;
		var content = '<span class="cnt">' + count + '</span>';
		$(element).closest('li').children('b').append(content);
	});

	$('#nav ul ul li:odd').addClass('odd');
	$('#nav ul ul li:even').addClass('even');

	$('#nav > ul > li > a').click(function() {

		var checkElement = $(this).next();

		$('#nav li').removeClass('active');
		$(this).closest('li').addClass('active');

		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			$(this).closest('li').removeClass('active');
			checkElement.slideUp('normal');
		}

		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$('#nav ul ul:visible').slideUp('normal');
			checkElement.slideDown('normal');
		}

		if($(this).closest('li').find('ul').children().length == 0) {
			return true;
		} else {
			return false;
		}

	});

	$('#nav > ul > li > b').click(function() {

		var checkElement = $(this).next();
		$('#nav ul li ul:visible').slideUp();


		$('#nav li').removeClass('active');
		$(this).closest('li').addClass('active'); 

		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			$(this).closest('li').removeClass('active');
			checkElement.slideUp('normal');
		}

		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$('#nav ul ul:visible').slideUp('normal');
			checkElement.slideDown('normal');
		}


		if($(this).closest('li').find('ul').children().length == 0) {
			return true;
		} else {
			return false;
		}

	});

	$('#nav > ul > li > ul > li > a').click(function() {

		var checkElement = $(this).next();

		$('#nav li').removeClass('active');
		$(this).closest('li').addClass('active');

		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			$(this).closest('li').removeClass('active');
			checkElement.slideUp('normal');
		}
		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$('#nav ul ul:visible').slideUp('normal');
			checkElement.slideDown('normal');
		}

		if($(this).closest('li').find('ul').children().length == 0) {
			return true;
		} else {
			return false;
		}

	});

	$('#partII').hide();
	$('#partIII').hide();
	$('partIV').hide();
	$(window).scroll(function () {
		if ($(document).height() <= $(window).scrollTop() + $(window).height()) {
			$('#partII').show();
			$(window).scroll(function () {
				if ($(document).height() <= $(window).scrollTop() + $(window).height()) {
					$('#partIII').show();
					$(window).scroll(function () {
						if ($(document).height() <= $(window).scrollTop() + $(window).height()) {
							$('#partIV').show();
						}
					});
				}
			});
		}
	});

	$('#nav > ul > li > ul > li > a').click(function(e){
		e.preventDefault();
		$.ajax({
			url: $(this).attr('href'),
			dataType: 'html',
			type: 'GET',
			success: function(data){
				console.log(data);
				$('#content').html(data);
			}
		});
		return false;
	});

	$('#nav > ul > li > b').click(function(e){
		e.preventDefault();
		$.ajax({
			url: $(this).attr('href'),
			dataType: 'html',
			type: 'GET',
			success: function(data){
				console.log(data);
				$('#content').html(data);
				$('#partII').hide();
				$('#partIII').hide();
			}
		});
		return false;
	})

	$('#albums').DataTable( {
		data: albums,
		columns: [
			{ title: "Nazwa" },
			{ title: "Wykonawca" },
			{ title: "Rok wydania" },
			{ title: "Wytwornia" }
		]
	} );
});
